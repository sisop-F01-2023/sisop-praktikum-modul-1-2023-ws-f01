#!/bin/bash

#REGISTER AN ACCOUNT

echo "Register yourself"

# Username entry
read -p "Username: " username
if grep -q "^${username}:" ./users/user.txt; then
	echo "User already registered. Please try again."
	echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User $username already exists." >> ./users/log.txt
	exit 0
fi

# Password entry
read -p "Password: " -s password

# Password requirements check
if [[ ${#password} -lt 8 || ! $password =~ ^[a-zA-Z0-9_]+$ || $password == $username || $password == *"chicken"* || $password == *"ernie"* || ! $password =~ [a-z] || ! $password =~ [A-Z] ]]; then
	echo " "
        if [[ ${#password} -lt 8 ]]; then
        	echo "Password should have more than 8 characters."
        fi
        if [[ ! $password =~ ^[a-zA-Z0-9_]+$ ]]; then
        	echo "Password should have atleast one number and one uppercase."
        fi
        if [[ $password == $username ]]; then
        	echo "Password shouldn't be the same as username."
        fi
        if [[ $password == *"chicken"* || $password == *"ernie"* ]]; then
        	echo "Password shouldn't contain 'chicken' or 'ernie'."
        fi
        echo "Your password doesn't fulfill the requirements. Please try again."
else
	echo " "
	echo "Registered succesfully."
	#Message to log.txt and user.txt
	echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully." >> ./users/log.txt
	echo "$username:$password:" >> ./users/user.txt
fi
