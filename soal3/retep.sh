#!/bin/bash

#LOGIN AN ACCOUNT

echo "Log in"

read -p "Username: " username
read -p "Password: " -s password

afkh=$(awk /^$username.*$password/' {print "1"}' ./users/user.txt)

if [[ "$afkh" -eq 1 ]]; then
	echo " "
	echo "Succesfully logged in."
	echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in." >> ./users/log.txt
else
	echo " "
	echo "Username or password is incorrect"
	echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username.
" >> ./users/log.txt

fi

