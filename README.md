# Laporan Resmi Praktikum Sistem Operasi Modul 1 Kelompok F01
## Anggota Kelompok
NRP | Nama |
--- | --- | 
5025211187 | Altriska Izzati Khairunnisa Hermawan |
5025211250 | Syomeron Ansell Widjaya |
5025211254 | Yusna Millaturrasyidah | 

## Daftar Isi
- [Soal 1](#soal-1)
	- [Penyelesaian soal 1](#penyelesaian-soal-1)
- [Soal 2](#soal-2)
	- [Penyelesaian soal 2](#penyelesaian-soal-2)
- [Soal 3](#soal-3)
	- [louis.sh](#louis)
	- [retep.sh](#retep)
- [Soal 4](#soal-4)
	- [log_encrypt.sh](#log_encrypt)
	- [log_decrypt.sh](#log_decrypt)
- [Kendala](#kendala)

## Soal 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
- Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang. 
- Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a. 
- Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi. 
- Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

### Penyelesaian soal 1
```sh
echo -e "5 Universitas dengan ranking tertinggi di Jepang:"
```
Menampilkan soal dengan perintah echo.
```sh
cat bochi.csv | grep "Japan" | head -n 5 | awk -F ',' '{print $2}'
```
File .csv dibaca dengan perintah `cat`. 
Perintah grep untuk mencari baris yang mengandung kata `Japan`. 
Perintah `head -n 5` untuk membatasi hasil pencarian hanya sampai 5 baris pertama. 
Perintah `awk -F ',' '{print $2}'` untuk menampilkan kolom kedua pada setiap baris yang dipisahkan dengan koma.
```sh
echo -e "\n5 Universitas di Jepang dengan Faculty Student Score(fsr score) yang paling rendah:"
```
Menampilkan soal dengan perintah echo.
```sh
cat bochi.csv | grep "Japan" | head -n 5  | sort -t, -nk9 | awk -F ',' '{print $2}'
```
File .csv dibaca dengan perintah `cat`. 
Perintah `grep` untuk mencari baris yang mengandung kata "Japan". 
Perintah `head -n 5` untuk membatasi hasil pencarian hanya sampai 5 baris pertama. 
Perintah `sort -t, -nk9` untuk mengurutkan baris-baris tersebut berdasarkan kolom kesembilan secara numerik.
Perintah `awk -F ',' '{print $2}'` untuk menampilkan kolom kedua pada setiap baris yang dipisahkan dengan koma.
```sh
echo -e "\n10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi:"
```
Menampilkan soal dengan perintah echo.
```sh
cat bochi.csv | grep "Japan" | sort -t, -grk19 | awk -F ',' '{print $2}' | head -n 10
```
File .csv dibaca dengan perintah `cat`. 
Perintah `grep` untuk mencari baris yang mengandung kata `Japan`. 
Perintah `sort -t, -grk19` untuk mengurutkan baris-baris tersebut berdasarkan kolom kesembilan secara numerik secara descending. 
Perintah `awk -F ',' '{print $2}'` untuk menampilkan kolom kedua pada setiap baris yang dipisahkan dengan koma. 
Perintah `head -n 10` untuk membatasi hasil pencarian hanya sampai 10 baris pertama.
```sh
echo -e "\nUniversitas dengan kata kunci Keren:"
```
Menampilkan soal dengan perintah `echo`.
```sh
cat bochi.csv | grep -i "keren" | awk -F ',' '{print $2}'
```
File .csv dibaca dengan perintah `cat`. 
Perintah `grep -i "keren"` untuk mencari baris yang mengandung kata "keren" secara case insensitive. 
Perintah `awk -F ',' '{print $2}'` untuk menampilkan kolom kedua pada setiap baris yang dipisahkan dengan koma.

#### Screenshot output
![Output_1](/uploads/d7bd1d9eb00c5f18d968f7d5371aff86/no_1.png)<br />

## Soal 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
- File yang didownload memilki format nama `perjalanan_NOMOR.FILE` Untuk `NOMOR.FILE`, adalah urutan file yang download (`perjalanan_1`, `perjalanan_2`, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama `kumpulan_NOMOR.FOLDER` dengan `NOMOR.FOLDER` adalah urutan folder saat dibuat (`kumpulan_1`, `kumpulan_2`, dst) 
- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip `devil_NOMOR ZIP` dengan `NOMOR.ZIP` adalah urutan folder saat dibuat (`devil_1`, `devil_2`, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

### Penyelesaian
Untuk soal no 2 karena ada 2 perintah maka untuk mengerjakannya dibuat dengan menggunakan 2 fungsi supaya dapat membedakan perintah untuk download file gambar dan melakukan zip folder.
```sh
download(){
count=1
while true
	do
	folder="kumpulan_$count"
		if [ -z "$(find ~/Documents/PraktikumSISOP/sisop-praktikum-modul-1-2023-ws-f01/soal2 -name "$folder")" ]
			then break;
		else ((count++))
		fi
	done
```
Potongan kode diatas berfungsi untuk mencari nama folder yang tersedia didalam direktori `~/Documents/PraktikumSISOP/sisop-praktikum-modul-1-2023-ws-f01/soal2`. Pertama menggunakan perintah while untuk melakukan perulangan. Kemudian mencari folder dalam direktori dengan pola nama `kumpulan_<nomor>`. Setiap kali melakukan perulangan, variabel `count` akan diincrement dan kemudian digunakan untuk membuat nama folder yang akan dicari.
Pada bagian `if statement`, script menggunakan perintah find untuk mencari folder dengan pola nama yang sesuai dalam direktori yang telah ditentukan. Apabila `find` tidak menemukan folder, maka perulangan akan dihentikan dengan menggunakan perintah `break`.
```sh
mkdir "kumpulan_$count"
```
Potongan kode diatas merupakan perintah untuk membuat direktori baru dengan nama yang dihasilkan dari gabungan string `kumpulan_` dan nilai variabel `count`
```sh
hour=$(date +"%H")
	for ((i=1; i<=$hour; i++))
	do 
		wget -O "kumpulan_$count/perjalanan_$i.jpg" "https://source.unsplash.com/random/800x600/?indonesia"
	done
}
```
Pada bagian `hour=$(date +"%H")`, perintah `date` digunakan untuk mendapatkan nilai dari variabel `hour` yang akan digunakan untuk menentukan jumlah gambar yang akan diunduh. Nilai `hour` menunjukkan jam pada sistem saat kode di run, sehingga jumlah gambar yang diunduh akan sama dengan jumlah jam saat itu. Selanjutnya, kode tersebut menggunakan perintah `for` untuk melakukan perulangan sebanyak `hour` kali.

Kemudian mengguankan perintah `wget` untuk mengunduh gambar dari situs web `https://source.unsplash.com/random/800x600/?indonesia`. Gambar-gambar tersebut akan disimpan di dalam folder dengan nama `kumpulan_<nomor>`, setiap gambar akan diberi nama `perjalanan_<nomor>.jpg`.
```sh
Zip(){
countzip=1
while [ -f "devil_$countzip.zip" ]
	do 
		((countzip++))
	done

	if [[ ! -d "devil_$countzip.zip" ]];
	then 
	find ~/Documents/PraktikumSISOP -type d -name "kumpulan*" -execdir zip -r "devil_$countzip.zip" {} \;
	fi
}
```
Potongan kode diatas berfungsi untuk mengompresi folder-folder dengan nama `kumpulan_<nomor>` menjadi file zip dan menyimpannya didalam direktori dengan nama `devil_<nomor>.zip`.
Pertama menggunakan perintah `while` untuk mencari file zip dengan nama `devil_<nomor>.zip` dalam direktori saat ini. Setiap kali dilakukan perulangan, variabel `countzip` akan diincrement dan kemudian digunakan untuk membuat nama file zip yang akan dicari. Kemudian pada bagian if statement, digunakan perintah `find` untuk mencari folder dengan nama `kumpulan_<nomor>` dalam direktori `~/Documents/PraktikumSISOP`. Lalu, perintah `zip` digunakan untuk mengompresi setiap folder menjadi file zip dengan nama `devil_<nomor>.zip`. Selanjutnya, file zip akan disimpan dalam direktori saat ini. 
```sh
if [[ $1 == "download" ]]
then
	download
elif [[ $1 == "Zip" ]]
then
	Zip
fi
```
Potongan kode diatas merupakan perintah untuk mengecek nilai argumen yang diberikan saat menjalankan script. Apabila argumen yang digunakan `download`, maka akan menjalankan semua kode yang ada didalam fungsi `download()`, Sedangkan jika argumen yang digunakan adalah `Zip`, maka yang akan dijalankan adalah semua kode yang ada didalam fungsi `Zip()`.


```sh
0 */10 * * * /home/yusnamilla/Documents/PraktikumSISOP/sisop-praktikum-modul-1-2023-ws-f01/soal2/kobeni_liburan.sh download
```
Crontab untuk menjalankan kobeni_liburan.sh download setiap 10 jam sekali

```sh
0 */24 * * * /home/yusnamilla/Documents/PraktikumSISOP/sisop-praktikum-modul-1-2023-ws-f01/soal2/kobeni_liburan.sh Zip
```
Crontab untuk menjalankan kobeni_liburan.sh Zip setiap 1 hari 

Perintah diatas merupakan cronjob yang digunakan untuk menjalankan script secara otomatis pada waktu tertentu.

#### Screenshot output
![Output_2_a](/uploads/b866850f14cb1df28d2a5ff829452ce5/no_2_a.png)

![Output_2_b](/uploads/a7aa60458f1d25aafffd4603031c8317/no_2_b.png)

## Soal 3
Peter Griffin hendak membuat suatu sistem register pada script `louis.sh` dari setiap user yang berhasil didaftarkan di dalam file `/users/users.txt`. Peter Griffin juga membuat sistem login yang dibuat di script `retep.sh`.
Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username 
- Tidak boleh menggunakan kata `chicken` atau `ernie`
Setiap percobaan login dan register akan tercatat pada `log.txt` dengan format : `YY/MM/DD hh:mm:ss MESSAGE`. Message pada log akan berbeda tergantung aksi yang dilakukan user.
- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah `REGISTER: ERROR User already exists`
- Ketika percobaan register berhasil, maka message pada log adalah `REGISTER: INFO User USERNAME registered successfully`
- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah `LOGIN: ERROR Failed login attempt on user USERNAME`
- Ketika user berhasil login, maka message pada log adalah `LOGIN: INFO User USERNAME logged in`


### Penyelesaian
Dibutuhkan dua file `.sh` untuk dapat menyelesaikan soal ini. Pertama adalah `louis.sh` untuk registrasi dan `retep.sh` untuk login.
#### **louis**
```sh
echo "Register yourself"
read -p "Username: " username
if grep -q "^${username}:" ./users/user.txt; then
	echo "User already registered. Please try again."
	echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User $username already exists." >> ./users/log.txt
	exit 0
fi
```
Meminta user untuk input username yang akan disimpan di variabel `username` setelah itu akan dicek apakah username yang diinput sama dengan username yang sudah terdaftar pada `/users/user.txt` menggunakan `if grep -q`. Jika diperhatikan pada `"^${username}:"` pengecekan username ditambahkan `:`, hal ini digunakan untuk memudahkan program untuk mencari username yang benar-benar sesuai. Apabila sama, maka akan muncul pesan kesalahan `"User already registered. Please try again."` diikuti dengan mencatat pesan kesalahan tersebut ke dalam file `log.txt` dan keluar dari program

```sh
read -p "Password: " -s password
```
Meminta user untuk input password. Karakter yang diinput oleh user akan disembunyikan dengan menggunakan `-s`, dengan ini password tidak akan ditampilkan di layar.

```sh
if [[ ${#password} -lt 8 || ! $password =~ ^[a-zA-Z0-9_]+$ || $password == $username || $password == *"chicken"* || $password == *"ernie"* || ! $password =~ [a-z] || ! $password =~ [A-Z] ]]; then
	echo " "
        if [[ ${#password} -lt 8 ]]; then
        	echo "Password should have more than 8 characters."
        fi
        if [[ ! $password =~ ^[a-zA-Z0-9_]+$ ]]; then
        	echo "Password should have atleast one number and one uppercase."
        fi
        if [[ $password == $username ]]; then
        	echo "Password shouldn't be the same as username."
        fi
        if [[ $password == *"chicken"* || $password == *"ernie"* ]]; then
        	echo "Password shouldn't contain 'chicken' or 'ernie'."
        fi
        echo "Your password doesn't fulfill the requirements. Please try again."
```
Mengecek apakah password yang dimasukkan memenuhi persyaratan yang telah ditentukan. Persyaratan tersebut adalah:
- Panjang password harus lebih dari 8 karakter. Dicek menggunakan `${#password} -lt 8`. Apabila kurang dari 8 karakter, maka akan muncul pesan `"Password should have more than 8 characters."`
- Harus terdiri dari huruf besar (uppercase), huruf kecil (lowercase), dan angka (digit). Dicek menggunakan `! $password =~ ^[a-zA-Z0-9_]+$`. Perhatikan bahwa ekspresi ini menggunakan `!` atau negasi sehingga apabila tidak memenuhi ekspresi ini maka akan bernilai `true`, mengakibatkan pesan `"Password should have atleast one number and one uppercase."` pada layar
- Password tidak boleh sama dengan nama pengguna. Dicek menggunakan `$password == $username`. Apabila sama akan menunjukkan pesan `"Password shouldn't be the same as username."`
- Tidak boleh mengandung kata "chicken" atau "ernie". Dicek menggunakan `$password == *"chicken"* || $password == *"ernie"*`. Apabila mengandung dua kata tersebut maka akan muncul pesan `"Password shouldn't contain 'chicken' or 'ernie'."`
Jika password tidak memenuhi persyaratan tersebut, maka akan muncul pesan yang sesuai dengan kondisi yang tidak terpenuhi, yaitu `"Your password doesn't fulfill the requirements. Please try again."`.
```sh
else
	echo " "
	echo "Registered succesfully."
	#Message to log.txt and user.txt
	echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully." >> ./users/log.txt
	echo "$username:$password:" >> ./users/user.txt
fi
```
Sebaliknya apabila password memenuhi persyaratan maka akan muncul pesan `"Registered succesfully."` dan mencatat pesan tersebut ke dalam file `log.txt`. Selain itu, program juga akan menambahkan nama pengguna dan kata sandi ke dalam file `user.txt` dengan format `"username:password:"`. Mengingat pengecekan username di atas, penambahan `:` setelah username dan password untuk memudahkan pengecekan.

#### Screenshot output
![Output_louis](/uploads/4686484176bb1b3bcc384bf8f417d6f2/louis.jpeg)
<br />
Username dan password yang diinput masuk ke dalam `user.txt`

#### **retep**
```sh
read -p "Username: " username
read -p "Password: " -s password
```
Perintah-perintah ini membaca input yang diberikan oleh user dan menyimpannya ke dalam variabel `username` dan `password` masing-masing. `-p` digunakan untuk menampilkan pesan prompt pada layar, sedangkan `-s` digunakan untuk menyembunyikan karakter input (password) yang dimasukkan.


```sh
afkh=$(awk /^$username.*$password/' {print "1"}' ./users/user.txt)
``` 
Mencari apakah username dan password yang dimasukkan oleh user sesuai dengan data yang ada pada file `user.txt`. `awk` adalah sebuah program untuk melakukan manipulasi teks, yang digunakan dalam hal ini untuk mencari pola tertentu pada sebuah file teks. Variabel `afkh` akan bernilai `1` jika username dan password yang dimasukkan oleh user cocok dengan data yang ada pada file `user.txt`.

```sh
if [[ "$afkh" -eq 1 ]]; then 
	echo " "
	echo "Succesfully logged in."
	echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in." >> ./users/log.txt
else
	echo " "
	echo "Username or password is incorrect"
	echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username.
" >> ./users/log.txt
fi
```
Jika nilai dari variabel `afkh` adalah `1`, maka pesan `"Successfully logged in."` akan ditampilkan pada layar, dan sebuah pesan sukses login akan ditulis ke dalam file `log.txt`. Jika nilai dari variabel `afkh` tidak sama dengan `1`, maka pesan `"Username or password is incorrect"` akan ditampilkan pada layar, dan sebuah pesan gagal login akan ditulis ke dalam file `log.txt`.

#### Screenshot output
![Output_retep](/uploads/4e51639e7948f64a4f025e350171f10f/retep.jpeg)
<br />
Aktivitas register dan login masuk ke dalam `log.txt`

## Soal 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File `syslog` tersebut harus memiliki ketentuan : 
- Backup file log system dengan format `jam:menit tanggal:bulan:tahun` (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file `syslog` setiap 2 jam untuk dikumpulkan

### Penyelesaian
Dibutuhkan dua file `.sh` untuk dapat menyelesaikan soal ini. Pertama adalah `log_encrypt.sh` untuk mengenkripsi file `syslog` dan `log_decrypt.sh` untuk mendekripsi file `syslog` yang telah dienkrip.

#### **log_encrypt**
Script ini berfungsi untuk melakukan enkripsi isi file log sistem `/var/log/syslog` dan menyimpan hasil enkripsi ke dalam folder tertentu `/home/altriska/Documents/sisop1/soal4/encrypt_folder` dengan format nama file berupa jam dan tanggal enkripsi dilakukan. Enkripsi dilakukan dengan menggunakan teknik Caesar Cipher dengan shift key yang ditentukan berdasarkan jam pada saat script dijalankan.

```sh
log_file="/var/log/syslog"
```
Variabel yang menyimpan path menuju file log sistem yang akan dienkripsi.

```sh
this_hour=$(date "+%H")
```
Variabel yang menyimpan nilai jam pada saat script dijalankan menggunakan perintah `date` dengan format `%H` dalam format 24-jam. Hasil dari perintah ini kemudian disimpan ke dalam variabel `this_hour`.

```sh
lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')
```
Dua variabel ini akan menyimpan set alfabet yang diulang selama dua kali untuk membuat sebuah siklus alfabet yang di-shift nantinya tidak terpotong pada bagian akhir.
Script ini membedakan lowercase alphabet dan uppercase alphabet karena dalam proses enkripsi, karakter-karakter pada log file yang berupa huruf akan diubah secara terpisah sesuai dengan huruf kapitalisasi.
Karena proses enkripsi menggunakan metode **Caesar Cipher**, di mana setiap karakter pada log file akan digeser sebanyak `n` huruf pada alfabet, dengan `n` adalah waktu pada saat proses enkripsi dilakukan. Oleh karena itu, perlu membedakan antara huruf kapital dan huruf kecil agar pergeseran karakter yang dilakukan dapat berbeda antara huruf kapital dan huruf kecil yang sama.

```sh
unshifted_lowercase=${lowercase_alphabet:0:26}
unshifted_uppercase=${uppercase_alphabet:0:26}
```
Menyimpan set alfabet yang masih belum digeser (shift key = 0) dengan menggunakan parameter expansion. Format parameter expansion pada script ini adalah `${variable_name:start_position:length}` yang berarti mengambil `length` karakter dari variabel `variable_name` mulai dari karakter pada posisi `start_position`.

```sh
shifted_lowercase=${lowercase_alphabet:${this_hour}:26}
shifted_uppercase=${uppercase_alphabet:${this_hour}:26}
```
Menyimpan set alfabet yang sudah digeser (shift key = nilai jam pada saat script dijalankan).

```sh
encrypted_file="/home/altriska/Documents/sisop1/soal4/encrypt_folder/$(date +%H:%M_%d:%m:%Y).txt"
```
Menyimpan path menuju file hasil enkripsi dengan format nama file berupa jam dan tanggal enkripsi dilakukan.

```sh
cat $log_file | tr "${unshifted_lowercase}${unshifted_uppercase}" "${shifted_lowercase}${shifted_uppercase}" > "${encrypted_file}"
```
Mengambil isi file log sistem, melakukan enkripsi dengan menggunakan teknik Caesar Cipher dengan shift key yang sudah ditentukan, dan menyimpan hasil enkripsi ke dalam file yang sudah ditentukan. Menggunakan perintah `tr` dengan menggeser setiap karakter sejauh `$this_hour` huruf ke depan. Lalu hasil enkripsi disimpan pada file baru `${encrypted_file}` dengan operator `>` (redirect output).
##### **Cronjob**
Digunakan **crontab** untuk backup file log sistem setiap dua jam secara otomatis, yaitu menjalankan script ini sekali setiap dua jam secara otomatis. Dengan mengetikkan `crontab -e` pada terminal dan menambahkan baris 
```sh
0 */2 * * * /bin/bash /home/altriska/sisop-praktikum-modul-1-2023-ws-f1/soal4/log_encrypt.sh
```

##### Screenshot output
![Output_encrypt_terminal](/uploads/1899a41aed389d58e273825df4e2d196/en_ter.jpeg)

![Output_encrypt_txt](/uploads/9687d642ceded94597e8716a0d4c1fa0/en_txt.jpeg)

#### **log_decrypt**
Script ini untuk melakukan dekripsi file yang sebelumnya telah dienkripsi menggunakan `log_encrypt.sh`. Pada dasarnya cara kerja script ini mirip dengan script `log_encrypt.sh`, perbedaannya adalah 
```sh
this_hour=$(date "+%H")
```
Mendapatkan jam pada saat script dijalankan.

```sh
encryptedfile=$(ls -t /home/altriska/Documents/sisop1/soal4/encrypt_folder | head -n1)
encryptedpath="/home/altriska/Documents/sisop1/soal4/encrypt_folder/${encryptedfile}"
decryptedpath="/home/altriska/Documents/sisop1/soal4/decrypt_folder/${encryptedfile}"
```
Mendapatkan path dari file terbaru dalam direktori `/home/altriska/Documents/sisop1/soal4/encrypt_folder` dengan menggunakan perintah `ls -t | head -n1`. Hasil dari perintah ini kemudian disimpan ke dalam variabel `encryptedfile`. Selanjutnya, path dari file yang akan didekripsi disimpan ke dalam variabel `encryptedpath` dan path dari file hasil dekripsi disimpan ke dalam variabel `decryptedpath`.

```sh
lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')
```
Menyimpan set alfabet yang diulang selama dua kali untuk membuat sebuah siklus alfabet yang digeser nantinya tidak terpotong pada bagian akhir.
```sh
shifted_lowercase=${lowercase_alphabet:${this_hour}:26}
shifted_uppercase=${uppercase_alphabet:${this_hour}:26}
```
Menyimpan set alfabet yang sudah digeser (shift key = nilai jam pada saat script dijalankan).

```sh
cat $encryptedpath | tr "${shifted_lowercase}${shifted_uppercase}" "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}"> "${decryptedpath}"

echo "decrypted."
```
Command `tr` akan melakukan translasi karakter pada file enkripsi menggunakan alfabet yang telah digeser. Hasil dari translasi tersebut kemudian akan disimpan pada file dekripsi menggunakan operator `>` .
##### **Cronjob**
Digunakan **crontab** untuk backup file log sistem setiap dua jam, yaitu menjalankan script ini sekali setiap dua jam secara otomatis. Dengan mengetikkan `crontab -e` pada terminal dan menambahkan baris 
```sh
0 */2 * * * /bin/bash /home/altriska/sisop-praktikum-modul-1-2023-ws-f1/soal4/log_decrypt.sh
```
#### Screenshot output
![Output_decrypt_terminal](/uploads/d61ce34c499dc58f914dc163f3cb254a/de_ter.jpeg)

![Output_decrypt_txt](/uploads/49d65719326a2fd9558a5b747667aad7/de_txt.jpeg)

## Kendala
Dalam pengerjaan praktikum Sistem Operasi modul 1 ditemukan beberapa kendala. Kendala-kendala tersebut adalah:
- Masih baru dalam menggunakan bahasa bash sehingga masih belum terbiasa dengan sintaksnya.
- Sempat bingung dengan GitLab karena baru pertama kali membuat dokumentasi menggunakan GitLab.

##### Kendala soal 1
*Tidak ada*

##### Kendala soal 2
*Tidak ada*

##### Kendala soal 3
- Kendala input username. Misal, akun dengan username `baeirene` telah terdaftar di `user.txt` dan saat register akun baru menggunakan username `irene` tidak bisa dilakukan karena set stringnya ada di dalam akun yang telah terdaftar. Kendala ini diatasi dengan membatasi username dan password yang diinput ke `user.txt` dengan suatu karakter, di script ini menggunakan `:`.

##### Kendala soal 4
- Perulangan alfabet. Sebelumnya hanya memakai satu set alfabet, tidak ada perulangan. Hal ini menyebabkan error saat encrypt dan decrypt karena setelah set alfabet digeser sesuai dengan `this_hour`, hasilnya banyak karakter yang seragam karena terpotong pada bagian akhir set alfabet. Kendala ini diatasi dengan melakukan perulangan set alfabet selama dua kali.
