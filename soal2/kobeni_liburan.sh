#!/bin/bash

download(){

count=1
while true
	do
	folder="kumpulan_$count"
		if [ -z "$(find ~/Documents/PraktikumSISOP/sisop-praktikum-modul-1-2023-ws-f01/soal2 -name "$folder")" ]
			then break;
		else ((count++))
		fi
	done
mkdir "kumpulan_$count"

hour=$(date +"%H")
	for ((i=1; i<=$hour; i++))
	do 
		wget -O "kumpulan_$count/perjalanan_$i.jpg" "https://source.unsplash.com/random/800x600/?indonesia"
	done
}

Zip(){
countzip=1
while [ -f "devil_$countzip.zip" ]
	do 
		((countzip++))
	done

	if [[ ! -d "devil_$countzip.zip" ]];
	then 
	find ~/Documents/PraktikumSISOP -type d -name "kumpulan*" -execdir zip -r "devil_$countzip.zip" {} \;
	fi
}

if [[ $1 == "download" ]]
then
	download
elif [[ $1 == "Zip" ]]
then
	Zip
fi

##crontab untuk menjalankan kobeni_liburan.sh download setiap 10 jam sekali
#0 */10 * * * /home/yusnamilla/Documents/PraktikumSISOP/sisop-praktikum-modul-1-2023-ws-f01/soal2/kobeni_liburan.sh download

##crontab untuk menjalankan kobeni_liburan.sh Zip setiap 1 hari 
#0 */24 * * * /home/yusnamilla/Documents/PraktikumSISOP/sisop-praktikum-modul-1-2023-ws-f01/soal2/kobeni_liburan.sh Zip
