echo -e "5 Universitas dengan ranking tertinggi di Jepang:"
cat bochi.csv | grep "Japan" | head -n 5 | awk -F ',' '{print $2}'
echo -e "\n5 Universitas di Jepang dengan Faculty Student Score(fsr score) yang paling rendah:"
cat bochi.csv | grep "Japan" | head -n 5  | sort -t, -nk9 | awk -F ',' '{print $2}'
echo -e "\n10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi:"
cat bochi.csv | grep "Japan" | sort -t, -grk19 | awk -F ',' '{print $2}' | head -n 10 
echo -e "\nUniversitas dengan kata kunci Keren:"
cat bochi.csv | grep -i "keren" | awk -F ',' '{print $2}'
