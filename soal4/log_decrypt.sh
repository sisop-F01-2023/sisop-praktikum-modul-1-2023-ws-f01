#!/bin/bash

# Mendapatkan jam saat ini
this_hour=$(date "+%H")

encryptedfile=$(ls -t /home/altriska/Documents/sisop1/soal4/encrypt_folder | head -n1)
encryptedpath="/home/altriska/Documents/sisop1/soal4/encrypt_folder/${encryptedfile}"
decryptedpath="/home/altriska/Documents/sisop1/soal4/decrypt_folder/${encryptedfile}"

# Mendefinisikan alfabet yang akan digunakan
lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

shifted_lowercase=${lowercase_alphabet:${this_hour}:26}
shifted_uppercase=${uppercase_alphabet:${this_hour}:26}

cat $encryptedpath | tr "${shifted_lowercase}${shifted_uppercase}" "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}"> "${decryptedpath}"

echo "decrypted."

# crontab backup 2 jam sekali
# 0 */2 * * * /bin/bash /home/altriska/sisop-praktikum-modul-1-2023-ws-f1/soal4/log_decrypt.sh
