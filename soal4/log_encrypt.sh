#!/bin/bash

# Set folder backup dan file log system
log_file="/var/log/syslog"
this_hour=$(date "+%H")

lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

unshifted_lowercase=${lowercase_alphabet:0:26}
unshifted_uppercase=${uppercase_alphabet:0:26}

shifted_lowercase=${lowercase_alphabet:${this_hour}:26}
shifted_uppercase=${uppercase_alphabet:${this_hour}:26}

encrypted_file="/home/altriska/Documents/sisop1/soal4/encrypt_folder/$(date +%H:%M_%d:%m:%Y).txt"

cat $log_file | tr "${unshifted_lowercase}${unshifted_uppercase}" "${shifted_lowercase}${shifted_uppercase}" > "${encrypted_file}"

echo "encrypted."

# crontab backup 2 jam sekali
# 0 */2 * * * /bin/bash /home/altriska/sisop-praktikum-modul-1-2023-ws-f1/soal4/log_encrypt.sh

